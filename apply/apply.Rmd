% `*appply()`
% Peng Yu
% TAMU

# `apply()`

`apply()` is a function that allows one to call a user-specified function on each row or each column of a matrix.

The usage is `apply(X, MARGIN, FUN, ...)`. The arguments are

- `X`: a matrix
- `FUN`: a function to be applied on rows or columns of the matrix.
- `MARGIN`: for a matrix `1` indicates rows, `2` indicates columns.

Here are some examples.

```{r}
x=matrix(1:6, nc=2)
apply(x, 1, sum)
apply(x, 2, sum)
apply(x, 1, function(x) {x})
t(apply(x, 1, function(x) {x}))
apply(x, 2, function(x) {x})
```

# `lapply()` and `sapply()`

Similar to `apply()`, `lapply()`, where `l` stands for list, is a function that allows one to call a user-specified function on each element of a list (or vector converted to a list).
The result is a list of `lapply()`. For example,

```{r}
lapply(list(1:3, 11:13), sum)
```

`sum()` is applied to a two-element list with 1:3 and 11:13 as the elements. It returns a list consisting of 6 and 36.

Sometimes, the list returned by `lapply()` may be simplified when the elements of the list are of the same dimension.
In this case, we can use `sapply()` (where `s` stands for simplify). For example,

```{r}
sapply(list(1:3, 11:13), sum)
```

Note that the returned value of `sapply()` is a vector now.

