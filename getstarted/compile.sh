#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

R --slave --no-save --no-restore --no-init-file <<-EOF
suppressWarnings(library(rmarkdown))
render('getstarted.Rmd', output_dir='.', quiet=T)
EOF

